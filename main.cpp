#include <windows.h>
#include<fstream>
#include<sstream>
#include<cmath>
HWND tekst;
HWND przycisk_wykres, przycisk_definicje, przycisk_wzory, przycisk_symulacja;
int dlugosc_fali = 380;
int d = 380;
/* This is where all the input to the window goes to */
using namespace std;
double rad(int x)
{
	return (x*M_PI)/180;
}
int kolor(double kat, double I0, double d, double fala)
{
	kat = rad(kat);
	double wynik = 0;
	wynik = ((M_PI*d)/fala)*sin(kat);
	if(wynik != 0)
		wynik = sin(wynik)/wynik;
	else
		wynik = 1;
	wynik = I0*pow(wynik,2);
	return wynik;
}
double sinc(double x)
{
	if(x == 0) return 1;
	return sin(x)/x;
}
POINT obroc(int x, int y, int sx, int sy, double kat)
{
	x = x-sx;
	y = y-sy;
	int x2 = x*cos(rad(kat))-y*sin(rad(kat));
	int y2 = x*sin(rad(kat))+y*cos(rad(kat));
	
	POINT p;
	p.x = x2+sx;
	p.y = y2+sy;
	
	return p;
}
LRESULT CALLBACK WndProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam) {
	switch(Message) {
		
		/* Upon destruction, tell the main thread to stop */
		case WM_DESTROY: {
			PostQuitMessage(0);
			break;
		}
		
		case WM_COMMAND: {
			switch(wParam)
			{
				case 1: {
					ifstream plik;
					plik.open("definicje.txt");
					if(plik.good() == false)
						MessageBox(hwnd, "Nie mo�na otworzy� pliku definicje.txt", "B��d", MB_ICONERROR|MB_OK);
					string zawartosc ="";
					string linia;
					while(getline(plik, linia))
					{
						zawartosc=zawartosc+linia+"\n";
					}
					plik.close();
					SetWindowText(tekst, zawartosc.c_str());
					break;
				}
				case 2: {
					ifstream plik;
					plik.open("wzory.txt");
					if(plik.good() == false)
						MessageBox(hwnd, "Nie mo�na otworzy� pliku wzory.txt", "B��d", MB_ICONERROR|MB_OK);
					string zawartosc ="";
					string linia;
					while(getline(plik, linia))
					{
						zawartosc=zawartosc+linia+"\n";
					}
					plik.close();
					SetWindowText(tekst, zawartosc.c_str());
					break;
				}
				case 3: {
					int srodekx = 145;
					int srodeky = 240;
					
					HDC hdc = GetDC(hwnd);
					Rectangle(hdc, 50, 210, 600, 630);
					MoveToEx(hdc, 145, 240, NULL);
					LineTo(hdc, 145, 495);
					LineTo(hdc, 505, 495);
					
					for(int i=0; i<=180; i++)
					{
						if(i%10 == 0)
						{
							MoveToEx(hdc, i*2+145, 491, NULL);
							LineTo(hdc, i*2+145, 499);
						}
						if(i%30==0)
						{
							int j = i;
							j = j-90;
							ostringstream ss;
							ss << j;
							string temp = ss.str();
							TextOut(hdc, i*2+140, 502, temp.c_str(), temp.size());
						}
					}
					
					for(int i=0; i<=255; i++)
					{
						if(i%10 ==0)
						{
							MoveToEx(hdc, 141, 495-i, NULL);
							LineTo(hdc, 149, 495-i);
						}
						
						if(i%30== 0 || i==255)
						{
							ostringstream s;
							s << i;
							string temp = s.str();
							TextOut(hdc, 115, 495-i+1, temp.c_str(), temp.size());
						}
					}
					
					double kat = -90;
					for(int i=0; i<=360; i++)
					{
						int y = round(kolor(kat, 255, d, dlugosc_fali));
						SetPixel(hdc, i+145, 495-y, RGB(255, 0, 0));
						kat+=0.5;
					}
					
					TextOut(hdc, 144, 220, "I", 1);
					TextOut(hdc, 520, 490, "Alfa", 4);
					
					ReleaseDC(hwnd, hdc);
					
					HWND temp = CreateWindowEx(WS_EX_CLIENTEDGE,"STATIC","Wykres pokazuje jasno�� w funkcji odchylenia od szczeliny\ndla k�ta Alfa",WS_VISIBLE|WS_CHILD|SS_CENTER,
						145, /* x */
						520, /* y */
						400, /* width */
						40, /* height */
						hwnd,NULL,NULL,NULL);
					break;
				}
				
				case 4: {
					HDC hdc = GetDC(hwnd);
					
					HBRUSH b = CreateSolidBrush(RGB(0, 0, 0));
					
					SelectObject(hdc, b);
					Rectangle(hdc, 50, 210, 600, 630);
					
					b = CreateSolidBrush(RGB(0, 255, 0));
					SelectObject(hdc, b);
					Rectangle(hdc, 51, 211, 200, 629);
					
					
					
					HPEN p = CreatePen(PS_SOLID, 1, RGB(0, 255, 0));
					SelectObject(hdc, p);
					
					double kat = -90;
					while(kat <= 90)
					{
						int k = kolor(kat, 255, d, dlugosc_fali);
						HPEN pen = CreatePen(PS_SOLID, 1, RGB(0, k, 0));
						POINT point = obroc(700, 421, 200, 421, kat);
						
						if(point.x > 600)
							point.x = 600;
						if(point.y > 630)
							point.y = 630;
						if(point.y < 210)
							point.y = 210;
						
						
						SelectObject(hdc, pen);
						MoveToEx(hdc, 200, 421, NULL);
						LineTo(hdc, point.x, point.y);
						
						kat+=0.1;
					}
					p = CreatePen(PS_SOLID, 1, RGB(255, 255, 255));
					SelectObject(hdc, p);
					MoveToEx(hdc, 200, 210, NULL);
					LineTo(hdc, 200, 420);
					MoveToEx(hdc, 200, 422, NULL);
					LineTo(hdc, 200, 630);
					ReleaseDC(hwnd, hdc);
					break;
				}
			}
			break;
		}
		
		/* All other messages (a lot of them) are processed using default procedures */
		default:
			return DefWindowProc(hwnd, Message, wParam, lParam);
	}
	return 0;
}

/* The 'main' function of Win32 GUI programs: this is where execution starts */
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
	WNDCLASSEX wc; /* A properties struct of our window */
	HWND hwnd; /* A 'HANDLE', hence the H, or a pointer to our window */
	MSG msg; /* A temporary location for all messages */

	/* zero out the struct and set the stuff we want to modify */
	memset(&wc,0,sizeof(wc));
	wc.cbSize		 = sizeof(WNDCLASSEX);
	wc.lpfnWndProc	 = WndProc; /* This is where we will send messages to */
	wc.hInstance	 = hInstance;
	wc.hCursor		 = LoadCursor(NULL, IDC_ARROW);
	
	/* White, COLOR_WINDOW is just a #define for a system color, try Ctrl+Clicking it */
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
	wc.lpszClassName = "WindowClass";
	wc.hIcon		 = LoadIcon(NULL, IDI_APPLICATION); /* Load a standard icon */
	wc.hIconSm		 = LoadIcon(NULL, IDI_APPLICATION); /* use the name "A" to use the project icon */

	if(!RegisterClassEx(&wc)) {
		MessageBox(NULL, "Window Registration Failed!","Error!",MB_ICONEXCLAMATION|MB_OK);
		return 0;
	}

	hwnd = CreateWindowEx(WS_EX_CLIENTEDGE,"WindowClass","Dyfrakcja �wiat�a",WS_VISIBLE|WS_OVERLAPPEDWINDOW,
		0, /* x */
		0, /* y */
		1200, /* width */
		700, /* height */
		NULL,NULL,hInstance,NULL);

	tekst = CreateWindowEx(WS_EX_CLIENTEDGE,"STATIC","",WS_VISIBLE|WS_CHILD|WS_BORDER,
		650, /* x */
		30, /* y */
		500, /* width */
		600, /* height */
		hwnd,NULL,hInstance,NULL);
	

	przycisk_definicje = CreateWindowEx(WS_EX_CLIENTEDGE,"BUTTON","Definicje",WS_VISIBLE|WS_CHILD,
		50, /* x */
		30, /* y */
		250, /* width */
		60, /* height */
		hwnd,(HMENU)1,hInstance,NULL);
		
	przycisk_wzory = CreateWindowEx(WS_EX_CLIENTEDGE,"BUTTON","Wzory",WS_VISIBLE|WS_CHILD,
		350, /* x */
		30, /* y */
		250, /* width */
		60, /* height */
		hwnd,(HMENU)2,hInstance,NULL);
		
	przycisk_wykres = CreateWindowEx(WS_EX_CLIENTEDGE,"BUTTON","Wykres",WS_VISIBLE|WS_CHILD,
		50, /* x */
		120, /* y */
		250, /* width */
		60, /* height */
		hwnd, (HMENU)3,hInstance,NULL);
		
	przycisk_symulacja = CreateWindowEx(WS_EX_CLIENTEDGE,"BUTTON","Symulacja",WS_VISIBLE|WS_CHILD,
		350, /* x */
		120, /* y */
		250, /* width */
		60, /* height */
		hwnd,(HMENU)4 ,hInstance,NULL);

	HDC hdc = GetDC(hwnd);
	Rectangle(hdc, 50, 210, 600, 630);
	ReleaseDC(hwnd, hdc);


	if(hwnd == NULL) {
		MessageBox(NULL, "Window Creation Failed!","Error!",MB_ICONEXCLAMATION|MB_OK);
		return 0;
	}

	/*
		This is the heart of our program where all input is processed and 
		sent to WndProc. Note that GetMessage blocks code flow until it receives something, so
		this loop will not produce unreasonably high CPU usage
	*/
	while(GetMessage(&msg, NULL, 0, 0) > 0) { /* If no error is received... */
		TranslateMessage(&msg); /* Translate key codes to chars if present */
		DispatchMessage(&msg); /* Send it to WndProc */
	}
	return msg.wParam;
}
